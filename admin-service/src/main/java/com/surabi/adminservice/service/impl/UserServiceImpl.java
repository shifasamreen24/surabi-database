package com.surabi.adminservice.service.impl;


import com.surabi.adminservice.entity.AuthoritiesEntity;
import com.surabi.adminservice.entity.UserEntity;
import com.surabi.adminservice.repository.AuthoritiesRepository;
import com.surabi.adminservice.repository.UserRepository;
import com.surabi.adminservice.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthoritiesRepository authoritiesRepository;

    // read methods
    @Override
    public List<UserEntity> getAllUsers(){
        return userRepository.findAll();
    }
    @Override
    public Optional<UserEntity> getAUserById(Long id){
        return userRepository.findById(id);
    }

    //add user
    @Override
    public void addAUser(UserEntity userEntity){
        // bcrypt password and add pair in user table
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encryptedPassword = passwordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(encryptedPassword);
        userRepository.saveAndFlush(userEntity);
        // add user in authority table
        AuthoritiesEntity authoritiesEntity = new AuthoritiesEntity();
        authoritiesEntity.setUsername(userEntity.getUsername());
        authoritiesEntity.setRole("ROLE_USER");
        authoritiesRepository.save(authoritiesEntity);

    }
    //delete user
    @Override
    public void deleteUserById(Long id){
        userRepository.deleteById(id);
    }
    @Override
    public void deleteAllUsers(){
        userRepository.deleteAll();
    }


}
