package com.surabi.adminservice.service;


import com.surabi.adminservice.entity.UserEntity;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    //read
    List<UserEntity> getAllUsers();
    Optional<UserEntity> getAUserById(Long id);
    //add
    void addAUser(UserEntity userEntity);
    //delete
    void deleteAllUsers();
    void deleteUserById(Long id);

}
