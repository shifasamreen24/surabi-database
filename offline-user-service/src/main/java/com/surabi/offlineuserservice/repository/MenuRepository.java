package com.surabi.offlineuserservice.repository;


import com.surabi.offlineuserservice.entity.MenuEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<MenuEntity, Long> {
    @Query( value = "SELECT m.price FROM MENU_ENTITY m WHERE m.id = :inputId",  nativeQuery = true)
    Double getMenuItemPrice(@Param("inputId") Long inputId);
}
