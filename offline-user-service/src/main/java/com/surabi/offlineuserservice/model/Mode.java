package com.surabi.offlineuserservice.model;

public enum Mode {
    CASH, CARD;
}
