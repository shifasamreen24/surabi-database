package com.surabi.userservice.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "authorities")
public class AuthoritiesEntity implements Serializable {
    @Id
    @NonNull
    @Column(unique=true)
    private String username;
    @NonNull
    @Column
    private String role;
}
