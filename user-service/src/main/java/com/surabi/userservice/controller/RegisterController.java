package com.surabi.userservice.controller;


import com.surabi.userservice.entity.AuthoritiesEntity;
import com.surabi.userservice.entity.UserEntity;
import com.surabi.userservice.model.UserDto;
import com.surabi.userservice.repository.AuthoritiesRepository;
import com.surabi.userservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Slf4j
@Controller
public class RegisterController {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuthoritiesRepository authoritiesRepository;

    @GetMapping(value = "/register")
    public ModelAndView register(ModelAndView model, UserDto userDto) {
        model.setViewName("register");
        model.addObject("userDto", userDto);
        return model;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister( ModelAndView modelAndView, UserDto userDto) {

        log.info("Inside controller");
        UserEntity userEntity = new UserEntity();
        userEntity.setEnabled(Boolean.TRUE);
        userEntity.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userEntity.setUsername(userDto.getUsername());

        AuthoritiesEntity authoritiesEntity = new AuthoritiesEntity();
        authoritiesEntity.setUsername(userEntity.getUsername());
        authoritiesEntity.setRole("ROLE_USER");
        userRepository.saveAndFlush(userEntity);
        authoritiesRepository.saveAndFlush(authoritiesEntity);
        log.info("Save successful");

        return "redirect:/login";
    }
}
